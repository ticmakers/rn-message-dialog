import * as React from 'react'
import {
  ImageSourcePropType,
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  ViewProps,
  ViewStyle,
} from 'react-native'

import { IButtonProps } from '@ticmakers-react-native/button'
import { IIconProps } from '@ticmakers-react-native/icon'
import { TypeComponent } from '@ticmakers-react-native/core'

/**
 * Type to define the prop source of the MessageDialog component
 */
export type TypeMessageDialogImageSource = ImageSourcePropType

/**
 * Type to define the language available
 */
export type TypeMessageDialogLang = 'es' | 'en' | 'fr' | 'fa'

/**
 * Interface to define the states of the MessageDialog component
 * @export
 * @interface IMessageDialogState
 */
export interface IMessageDialogState {
  /**
   * A array of buttons for the actions of the dialog
   * @type {(TypeComponent[] & IButtonProps[])}
   * @memberof IMessageDialogState
   */
  actions?: TypeComponent[] & IButtonProps[]

  /**
   * A React-Native component or a string to define the description
   * @type {(string | TypeComponent)}
   * @memberof IMessageDialogState
   */
  description?: string | TypeComponent

  /**
   * Set true to use the error style
   * @type {boolean}
   * @memberof IMessageDialogState
   * @default false
   */
  error?: boolean

  /**
   * A React-Native component or an object of IconProps to define the icon
   * @type {(IIconProps | TypeComponent)}
   * @memberof IMessageDialogState
   */
  icon?: IIconProps | TypeComponent

  /**
   * Set true to use the success style
   * @type {boolean}
   * @memberof IMessageDialogState
   */
  success?: boolean

  /**
   * A React-Native component or a string to define the title
   * @type {(string | TypeComponent)}
   * @memberof IMessageDialogState
   */
  title: string | TypeComponent
}

/**
 * Interface to define the props of the MessageDialog component
 * @export
 * @interface IMessageDialogProps
 * @extends {IMessageDialogState}
 */
export interface IMessageDialogProps extends IMessageDialogState {
  /**
   *
   * Prop for group all the props of the MessageDialog component
   * @type {IMessageDialogState}
   * @memberof IMessageDialogProps
   */
  options?: IMessageDialogState
}

/**
 * Class to define the MessageDialog component
 * @class MessageDialog
 * @extends {React.Component<IMessageDialogProps, IMessageDialogState>}
 */
declare class MessageDialog extends React.Component<IMessageDialogProps, IMessageDialogState> {
  /**
   * Static method to show a message in a dialog
   * @static
   * @param {IMessageDialogProps} props     An object of the MessageDialogProps
   * @memberof MessageDialog
   */
  public static show(props: IMessageDialogProps): void

  /**
   * Static method to dismiss a message showed
   * @static
   * @memberof MessageDialog
   */
  public static dismiss(): void

  /**
   * Static method to render the icon component
   * @static
   * @param {(TypeComponent | IIconProps)} props      A React-Native component or an object of the IconProps
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Icon(props?: TypeComponent | IIconProps): TypeComponent

  /**
   * Static method that renders the title component
   * @static
   * @param {(TypeComponent | string)} props      A React-Native component or a string
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Title(props: TypeComponent | string): TypeComponent

  /**
   * Static method that renders the description component
   * @static
   * @param {(TypeComponent | string)} props      A React-Native component or a string
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Description(props: TypeComponent | string): TypeComponent
}

declare module '@ticmakers-react-native/message-dialog'

export default MessageDialog
