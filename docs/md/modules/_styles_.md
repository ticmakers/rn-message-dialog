[@ticmakers-react-native/message-dialog](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)

### Object literals

* [colors](_styles_.md#colors)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    borderRadius: 15,
    padding: 8,
  },
  containerError: {
    backgroundColor: colors.error,
  },
  containerSuccess: {
    backgroundColor: colors.success,
  },

  icon: {
    color: 'white',
    fontSize: 80,
  },
  iconContainer: {
    marginBottom: 24,
  },

  title: {
    color: 'white',
    fontSize: 26,
    fontWeight: 'bold',
  },
  titleContainer: {
    marginBottom: 16,
  },

  description: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  descriptionContainer: {
    marginBottom: 28,
  },

  actionsContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
})

*Defined in styles.ts:8*

#### Type declaration

 actionsContainer: `object`

 alignItems: "center"

 justifyContent: "center"

 container: `object`

 borderRadius: `number`

 padding: `number`

 containerError: `object`

 backgroundColor: `string`

 containerSuccess: `object`

 backgroundColor: `string`

 description: `object`

 color: `string`

 fontSize: `number`

 textAlign: "center"

 descriptionContainer: `object`

 marginBottom: `number`

 icon: `object`

 color: `string`

 fontSize: `number`

 iconContainer: `object`

 marginBottom: `number`

 title: `object`

 color: `string`

 fontSize: `number`

 fontWeight: "bold"

 titleContainer: `object`

 marginBottom: `number`

___

## Object literals

<a id="colors"></a>

### `<Const>` colors

**colors**: *`object`*

*Defined in styles.ts:3*

<a id="colors.error"></a>

####  error

**● error**: *`string`* = "#DA4453"

*Defined in styles.ts:4*

___
<a id="colors.success"></a>

####  success

**● success**: *`string`* = "#8CC152"

*Defined in styles.ts:5*

___

___

