
TIC Makers - React Native Message Dialog
========================================

React native component for message dialog.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Message Dialog Expo's snack

Install
-------

Install `@ticmakers-react-native/message-dialog` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/message-dialog --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/message-dialog
```

How to use?
-----------

```javascript
import React from 'react'
import MessageDialog from '@ticmakers-react-native/message-dialog'

export default class App extends React.Component {

  render() { }
}
```

Properties
----------

Name

Type

Default Value

Definition

\----

\----

\----

\----

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Create tests
*   Add new props and components in readme
*   Improve README

Version 1.0.2 ([Changelog](https://bitbucket.org/ticmakers/rn-message-dialog/src/master/CHANGELOG.md))
------------------------------------------------------------------------------------------------------

## Index

### External modules

* ["MessageDialog"](modules/_messagedialog_.md)
* ["index"](modules/_index_.md)
* ["styles"](modules/_styles_.md)

---

