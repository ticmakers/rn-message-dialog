[@ticmakers-react-native/message-dialog](../README.md) > ["MessageDialog"](../modules/_messagedialog_.md) > [MessageDialog](../classes/_messagedialog_.messagedialog.md)

# Class: MessageDialog

Class to define the MessageDialog component

*__class__*: MessageDialog

*__extends__*: {React.Component<IMessageDialogProps, IMessageDialogState>}

## Type parameters
#### SS 
## Hierarchy

 `Component`<`IMessageDialogProps`, `IMessageDialogState`>

**↳ MessageDialog**

## Index

### Constructors

* [constructor](_messagedialog_.messagedialog.md#constructor)

### Properties

* [context](_messagedialog_.messagedialog.md#context)
* [props](_messagedialog_.messagedialog.md#props)
* [refs](_messagedialog_.messagedialog.md#refs)
* [state](_messagedialog_.messagedialog.md#state)
* [contextType](_messagedialog_.messagedialog.md#contexttype)

### Methods

* [UNSAFE_componentWillMount](_messagedialog_.messagedialog.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_messagedialog_.messagedialog.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_messagedialog_.messagedialog.md#unsafe_componentwillupdate)
* [componentDidCatch](_messagedialog_.messagedialog.md#componentdidcatch)
* [componentDidMount](_messagedialog_.messagedialog.md#componentdidmount)
* [componentDidUpdate](_messagedialog_.messagedialog.md#componentdidupdate)
* [componentWillMount](_messagedialog_.messagedialog.md#componentwillmount)
* [componentWillReceiveProps](_messagedialog_.messagedialog.md#componentwillreceiveprops)
* [componentWillUnmount](_messagedialog_.messagedialog.md#componentwillunmount)
* [componentWillUpdate](_messagedialog_.messagedialog.md#componentwillupdate)
* [forceUpdate](_messagedialog_.messagedialog.md#forceupdate)
* [getSnapshotBeforeUpdate](_messagedialog_.messagedialog.md#getsnapshotbeforeupdate)
* [render](_messagedialog_.messagedialog.md#render)
* [setState](_messagedialog_.messagedialog.md#setstate)
* [shouldComponentUpdate](_messagedialog_.messagedialog.md#shouldcomponentupdate)
* [Description](_messagedialog_.messagedialog.md#description)
* [Icon](_messagedialog_.messagedialog.md#icon)
* [Title](_messagedialog_.messagedialog.md#title)
* [dismiss](_messagedialog_.messagedialog.md#dismiss)
* [show](_messagedialog_.messagedialog.md#show)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new MessageDialog**(props: *`Readonly`<`IMessageDialogProps`>*): [MessageDialog](_messagedialog_.messagedialog.md)

⊕ **new MessageDialog**(props: *`IMessageDialogProps`*, context?: *`any`*): [MessageDialog](_messagedialog_.messagedialog.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:428*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `Readonly`<`IMessageDialogProps`> |

**Returns:** [MessageDialog](_messagedialog_.messagedialog.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:430*

*__deprecated__*: 

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `IMessageDialogProps` |
| `Optional` context | `any` |

**Returns:** [MessageDialog](_messagedialog_.messagedialog.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:428*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
// For TS pre-3.7:
context!: React.ContextType<typeof MyContext>
// For TS 3.7 and above:
declare context: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<`IMessageDialogProps`> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:453*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:459*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<`IMessageDialogState`>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:454*

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:641*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<`IMessageDialogProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:673*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IMessageDialogProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<`IMessageDialogProps`>*, nextState: *`Readonly`<`IMessageDialogState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:701*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IMessageDialogProps`> |
| nextState | `Readonly`<`IMessageDialogState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:570*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:549*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<`IMessageDialogProps`>*, prevState: *`Readonly`<`IMessageDialogState`>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:612*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IMessageDialogProps`> |
| prevState | `Readonly`<`IMessageDialogState`> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:627*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<`IMessageDialogProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:656*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IMessageDialogProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:565*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<`IMessageDialogProps`>*, nextState: *`Readonly`<`IMessageDialogState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:686*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IMessageDialogProps`> |
| nextState | `Readonly`<`IMessageDialogState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:445*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<`IMessageDialogProps`>*, prevState: *`Readonly`<`IMessageDialogState`>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:606*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IMessageDialogProps`> |
| prevState | `Readonly`<`IMessageDialogState`> |

**Returns:** `SS` \| `null`

___
<a id="render"></a>

###  render

▸ **render**(): `ReactNode`

*Inherited from Component.render*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:446*

**Returns:** `ReactNode`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:440*

**Type parameters:**

#### K :  `keyof IMessageDialogState`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<`IMessageDialogProps`>*, nextState: *`Readonly`<`IMessageDialogState`>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/MessageDialog/node_modules/@types/react/index.d.ts:560*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IMessageDialogProps`> |
| nextState | `Readonly`<`IMessageDialogState`> |
| nextContext | `any` |

**Returns:** `boolean`

___
<a id="description"></a>

### `<Static>` Description

▸ **Description**(props: *`TypeComponent` \| `string`*): `TypeComponent`

*Defined in MessageDialog.tsx:100*

Static method that renders the description component

*__static__*: 

*__memberof__*: MessageDialog

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | `TypeComponent` \| `string` |  A React-Native component or a string |

**Returns:** `TypeComponent`

___
<a id="icon"></a>

### `<Static>` Icon

▸ **Icon**(props?: *`TypeComponent` \| `IIconProps`*): `TypeComponent`

*Defined in MessageDialog.tsx:62*

Static method to render the icon component

*__static__*: 

*__memberof__*: MessageDialog

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| `Optional` props | `TypeComponent` \| `IIconProps` |  A React-Native component or an object of the IconProps |

**Returns:** `TypeComponent`

___
<a id="title"></a>

### `<Static>` Title

▸ **Title**(props: *`TypeComponent` \| `string`*): `TypeComponent`

*Defined in MessageDialog.tsx:85*

Static method that renders the title component

*__static__*: 

*__memberof__*: MessageDialog

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | `TypeComponent` \| `string` |  A React-Native component or a string |

**Returns:** `TypeComponent`

___
<a id="dismiss"></a>

### `<Static>` dismiss

▸ **dismiss**(): `void`

*Defined in MessageDialog.tsx:51*

Static method to dismiss a message showed

*__static__*: 

*__memberof__*: MessageDialog

**Returns:** `void`

___
<a id="show"></a>

### `<Static>` show

▸ **show**(props: *`IMessageDialogProps`*): `void`

*Defined in MessageDialog.tsx:24*

Static method to show a message in a dialog

*__static__*: 

*__memberof__*: MessageDialog

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | `IMessageDialogProps` |  An object of the MessageDialogProps |

**Returns:** `void`

___

