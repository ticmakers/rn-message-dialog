import { StyleSheet } from 'react-native'

const colors = {
  error: '#DA4453',
  success: '#8CC152',
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 15,
    padding: 8,
  },
  containerError: {
    backgroundColor: colors.error,
  },
  containerSuccess: {
    backgroundColor: colors.success,
  },

  icon: {
    color: 'white',
    fontSize: 80,
  },
  iconContainer: {
    marginBottom: 24,
  },

  title: {
    color: 'white',
    fontSize: 26,
    fontWeight: 'bold',
  },
  titleContainer: {
    marginBottom: 16,
  },

  description: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  descriptionContainer: {
    marginBottom: 28,
  },

  actionsContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default styles
