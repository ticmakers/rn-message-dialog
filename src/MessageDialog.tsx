import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Title } from 'native-base'

// import { Col, Grid, Row } from '@ticmakers-react-native/flexbox'
import { AppHelper, TypeComponent } from '@ticmakers-react-native/core'
import Icon, { IIconProps } from '@ticmakers-react-native/icon'
import DialogManager, { DialogContent } from '@ticmakers-react-native/dialog'
import { IMessageDialogProps, IMessageDialogState } from './../index'
import styles from './styles'

/**
 * Class to define the MessageDialog component
 * @class MessageDialog
 * @extends {React.Component<IMessageDialogProps, IMessageDialogState>}
 */
export default class MessageDialog extends React.Component<IMessageDialogProps, IMessageDialogState> {
  /**
   * Static method to show a message in a dialog
   * @static
   * @param {IMessageDialogProps} props     An object of the MessageDialogProps
   * @memberof MessageDialog
   */
  public static show(props: IMessageDialogProps) {
    const containerStyle = StyleSheet.flatten([
      styles.container,
      props.success && styles.containerSuccess,
      props.error && styles.containerError,
    ])

    DialogManager.show({
      animation: 'fade',
      style: containerStyle,

      children: (
        <DialogContent>
          <View style={ styles.iconContainer }>{ this.Icon(props.icon) }</View>
          <View style={ styles.titleContainer }>{ this.Title(props.title) }</View>
          <View style={ styles.descriptionContainer }>{ this.Description(props.description) }</View>
          <View style={ styles.actionsContainer }>{ props.actions }</View>
        </DialogContent>
      ),
    })
  }

  /**
   * Static method to dismiss a message showed
   * @static
   * @memberof MessageDialog
   */
  public static dismiss(): void {
    DialogManager.destroy()
  }

  /**
   * Static method to render the icon component
   * @static
   * @param {(TypeComponent | IIconProps)} props      A React-Native component or an object of the IconProps
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Icon(props?: TypeComponent | IIconProps): TypeComponent {
    if (AppHelper.isComponent(props)) {
      return props as TypeComponent
    // tslint:disable-next-line: no-else-after-return
    } else if (!AppHelper.isComponent(props) && props && Object.keys(props).length > 0) {
      const _props: IIconProps = {
        color: styles.icon.color,
        size: styles.icon.fontSize,
        ...props as IIconProps,
      }
      return <Icon { ..._props } />
    }

    return <Icon name="check-circle" type="material-community" size={ styles.icon.fontSize } color={ styles.icon.color } />
  }

  /**
   * Static method that renders the title component
   * @static
   * @param {(TypeComponent | string)} props      A React-Native component or a string
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Title(props: TypeComponent | string): TypeComponent {
    if (AppHelper.isComponent(props)) {
      return props as TypeComponent
    }

    return <Title style={ styles.title }>{ props }</Title>
  }

  /**
   * Static method that renders the description component
   * @static
   * @param {(TypeComponent | string)} props      A React-Native component or a string
   * @returns {TypeComponent}
   * @memberof MessageDialog
   */
  public static Description(props: TypeComponent | string): TypeComponent {
    if (AppHelper.isComponent(props)) {
      return props as TypeComponent
    }

    return <Text style={ styles.description }>{ props }</Text>
  }
}
