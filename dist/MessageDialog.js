"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var native_base_1 = require("native-base");
var core_1 = require("@ticmakers-react-native/core");
var icon_1 = require("@ticmakers-react-native/icon");
var dialog_1 = require("@ticmakers-react-native/dialog");
var styles_1 = require("./styles");
var MessageDialog = (function (_super) {
    __extends(MessageDialog, _super);
    function MessageDialog() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MessageDialog.show = function (props) {
        var containerStyle = react_native_1.StyleSheet.flatten([
            styles_1.default.container,
            props.success && styles_1.default.containerSuccess,
            props.error && styles_1.default.containerError,
        ]);
        dialog_1.default.show({
            animation: 'fade',
            style: containerStyle,
            children: (React.createElement(dialog_1.DialogContent, null,
                React.createElement(react_native_1.View, { style: styles_1.default.iconContainer }, this.Icon(props.icon)),
                React.createElement(react_native_1.View, { style: styles_1.default.titleContainer }, this.Title(props.title)),
                React.createElement(react_native_1.View, { style: styles_1.default.descriptionContainer }, this.Description(props.description)),
                React.createElement(react_native_1.View, { style: styles_1.default.actionsContainer }, props.actions))),
        });
    };
    MessageDialog.dismiss = function () {
        dialog_1.default.destroy();
    };
    MessageDialog.Icon = function (props) {
        if (core_1.AppHelper.isComponent(props)) {
            return props;
        }
        else if (!core_1.AppHelper.isComponent(props) && props && Object.keys(props).length > 0) {
            var _props = __assign({ color: styles_1.default.icon.color, size: styles_1.default.icon.fontSize }, props);
            return React.createElement(icon_1.default, __assign({}, _props));
        }
        return React.createElement(icon_1.default, { name: "check-circle", type: "material-community", size: styles_1.default.icon.fontSize, color: styles_1.default.icon.color });
    };
    MessageDialog.Title = function (props) {
        if (core_1.AppHelper.isComponent(props)) {
            return props;
        }
        return React.createElement(native_base_1.Title, { style: styles_1.default.title }, props);
    };
    MessageDialog.Description = function (props) {
        if (core_1.AppHelper.isComponent(props)) {
            return props;
        }
        return React.createElement(react_native_1.Text, { style: styles_1.default.description }, props);
    };
    return MessageDialog;
}(React.Component));
exports.default = MessageDialog;
//# sourceMappingURL=MessageDialog.js.map